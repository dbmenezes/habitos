import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UsuariosSchema = Usuarios & Document;

@Schema()
export class Usuarios {
  @Prop()
  name: string;

  @Prop()
  age: number;

  @Prop()
  breed: string;
}

export const UsuariosSchema = SchemaFactory.createForClass(Usuarios);